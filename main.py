#import win32com.client
#from win32comext.shell import shellcon, shell
from pathlib import Path
from win32utils import *
from megautils import *


# by this point, we know...
# A. IShellFolder.BindToObject() can only bind to a direct child
# B. we have a way to get a PIDL from a display name, using shell.SHParseDisplayName() or shell.SHILCreateFromPath()
# C. we can get the PIDL of an object, using shell.SHGetIDListFromObject()




def main():
    print(f"environmental path: {MEGAPATH}")

    email = "mcsoudgj@gmail.com"
    password = r"!Q*Fnw@Kv5Z"
    print("====making sure we're logged in====")
    mega_login(email, password)
    print("====calling \"mega-cd /\"====")
    mega_cd("/")
    print("====calling mega-pwd====")
    mega_pwd()
    print("====now getting mega filesystem====")
    # ▼ load from cache if available, else build tree and put it into cache
    megaFs = (loadFromCache() if os.path.exists(r".\megaDirectoryCache.pkl") else
              (lambda: (lambda temp: (cacheDirectoryInPickle(temp), temp)[1])(buildTreeFromMega()))()
              )
    #print(f'megaFs: {megaFs}', flush=True)
    print("====now finding duplicates====")
    duplicates = getDuplicateFiles(megaFs, r"D:\Pictures\pre-dayton photo storage")
    showCursor()
    # TODO create a function to do something with these duplicates
    while True:
        usrCode = input(">>>")
        exec(usrCode)




if __name__ == '__main__':
    main()

def testCmd():
    cmd = """
    cmd /V:ON /s /c "\
    set var=txt & \
    echo hello & \
    echo var: !var! & \
    set var=newText & \
    echo var: !var! & \
    echo turning echo off... & \
    echo off & \
    echo now testing a for-loop... & \
    for /l %i in (0, 1, 5) do (\
        echo i: %i !var! & \
        echo second line in for-loop \
    )
    """
    return cmd

# takes in an IShellFolder bound to iPhone as input, and prints out iPhone contents




def pidlPathDisplay(shellFolder, pidl):
    return shellFolder.GetDisplayNameOf(pidl, shellcon.SHGDN_FORADDRESSBAR)

def pidlDisplay(shellFolder, pidl):
    return shellFolder.GetDisplayNameOf(pidl, shellcon.SHGDN_NORMAL)

# given IShellFolder object and pidl, returns IShellFolder object bound to pidl
def cd(IShellFolder, pidl):
    return IShellFolder.BindToObject(pidl, None, shell.IID_IShellFolder)

def getPidlPath(pidl):
    folder = shell.SHGetDesktopFolder()
    return folder.GetDisplayNameOf(pidl, shellcon.SHGDN_FORADDRESSBAR)

def getFolderAt(path):
    pidl = shell.SHParseDisplayName(path)
    return pidl

def iterateIphone(path, depth=None):
    if depth is None:
        print("please give a depth")
        return None
    path = Path(path)  # our path is now stored as a list
    print(f"parsed path: {[part for part in path.parts]}")
    com = win32com.client.Dispatch("shell.Application")  # initializes a shell object
    current = None  # this is going to represent the current folder, through which we'll iterate and enumerate items
    items = None  # will hold current.Items(), so that the thing we're updating (current) isn't the for-loop iterator
    progress = 0  # tracks how far into the path we've looked. This is how we avoid exceeding the input depth.

    # dict, with the int const for each sepcial folder in the system as {specialFoldername: intConst}
    shConst = {str(com.NameSpace(specialFolderConst)): specialFolderConst for specialFolderConst in range(0, 255)}
    foundNext = False  # we assume we can't find next, until proven otherwise
    pathPrefix = []  # list of parts of path already covered
    for i, part in enumerate(path.parts):  # iterate through different parts of the path
        # print(f"top of loop. part: {part}")
        # print(f"path.parts[i+1]: {path.parts[i+1]}")
        # print(f"type(part): {type(part)}")
        isLastPart = i + 1 >= len(path.parts)
        if progress >= depth:
            print("reached max depth")
            return None
        if part in shConst:  # if special folder, we bind current to it with its constant. I /think/ that this will always be the beginning???
            current = com.NameSpace(shConst[part])
            items = current.Items()
        print(f"==========={current}===========")
        try:
            for item in items:  # iterate through different items listed in each part of the path
                print(f"{item.Name}: {item.Type} - path: {item.Path}")
                if item.Type == "DCF":  # this if-statement is just for debugging.
                    print(f"DCF members: {dir(item)}")
                if not isLastPart:  # seeing if we've found the next part of the path
                    if item.Name in path.parts[i + 1]:
                        print(f"found next part of path!!! {item.Name}")
                        try:
                            assert not foundNext, "foundNext already set true before matching item"
                            try:
                                assert hasattr(item,
                                               'Path'), f"part {part}, item {item.Name} lacks 'Path' attribute"
                                print(f"now binding current to path {item.Path}")
                                current = com.NameSpace(item.Path)
                                print(f"current now set to {current}")
                            except AssertionError as pathAttrError:
                                return pathAttrError
                        except AssertionError as e:
                            print(f"something went wrong. next part of path found while foundNext == True")
                            return e
                        foundNext = True
                else:  # case where we've gotten to the end of the path. this /might/ be unnecessary
                    if item.Name in part:
                        print(f"found final part of path!!!...{item.Name}")
                        try:
                            assert not foundNext, "foundNext already set true before matching item"
                        except AssertionError as e:
                            print(f"something went wrong. next part of path found while foundNext == True")
                            return e
                        foundNext = True

            progress += 1
            pathPrefix.append(part)
        except Exception as e2:
            print(f"error in enumerating/searching items in part: {part}")
            return e2
        if not foundNext:
            print(f"unable to find next part of path.")
            print(f"total progress: {Path(*pathPrefix)}")
            return None
        foundNext = False
        items = current.Items()  # setting up items iterator for the next part of the path
    return pathPrefix == path

