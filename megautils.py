import os
from subprocess import Popen, PIPE
import pickle
import msvcrt
import time
import ctypes
from ctypes import wintypes

# come back and change this later. hard-coding path of mega commands
# because Windows likes to be a little bit silly and require you to
# enter paths to run batch scripts, even if  already in PATH 🤣😒😒😒🤦‍♂️🤪😩

# location of megacmd files on any Windows machine
MEGAPATH = (lambda localPath, globalPath: ';'.join([dir for dir in localPath.split(';') if dir.lower().endswith('megacmd')] +
                                                   [dir for dir in globalPath.split(';') if dir.lower().endswith('megacmd') and dir not in localPath.split(';')]))(os.environ.get('LOCAL_PATH', ''), os.environ.get('PATH', ''))





class Item:
    def __init__(self, **kwargs):
        for key, val in kwargs.items():
            setattr(self, key, val)

    def __repr__(self):
        return str([(key, self.__dict__[key]) for key in self.__dict__])


class Directory:
    def __init__(self, **kwargs):
        self._setAttr("_children", {})  # Store child directories
        self._setAttr("_metadata", Item(**kwargs))  # Store metadata

    def _setAttr(self, attr, val):
        """
        NOTE: should usually only be used internally in class
        takes in string `attr` and any-type `val`, and sets those internal variables directly
        (_children or _metadata)
        returns value given by super().__setattr__
        """
        return super().__setattr__(attr, val)

    def _getAttr(self, attr):
        """
        takes in a string for the attribute you want to get (_metadata or _children)
        returns the internal attributes (_metadata or _children) directly
        """
        return super().__getattribute__(attr)

    # makes it so that you can do root["subfolder"] = Directory() to make a child
    def __setitem__(self, key, val):
        if isinstance(val, Directory):
            self._getAttr("_children")[key] = val
        else:
            raise ValueError("Value must be an instance of Directory")

    # makes it so that you can do x = root["subfolder"]
    def __getitem__(self, child):
        children = self._getAttr("_children")
        if child in children:
            return children[child]
        raise KeyError(f"Child directory '{child}' does not exist")

    # makes it so that you can do root["subfolder"].size = 3
    def __setattr__(self, field, val):
        try:
            return setattr(self._metadata, field, val)
        except Exception as e:
            return e

    # makes it so that you can do x = root["subfolder"].size
    def __getattr__(self, field):
        return getattr(self._metadata, field)


    def __repr__(self):
        return f'Directory({self._getAttr("_metadata")}) with children {list(self._getAttr("_children").keys())}'

    def __iter__(self):
        return iter(self._getAttr("_children"))

    def keys(self):
        return self._getAttr("_children").keys()

    def __getstate__(self):
        # Return the object's state as a dictionary
        return self.__dict__.copy()

    def __setstate__(self, state):
        self.__dict__.update(state)

    # searches for file or folder
    def search(self, target):
        queue = [self]
        while len(queue) > 0:
            current = queue.pop(0)
            if current.name == target:
                return current
            children = current._getAttr("_children")
            queue.extend(children.keys())
        return None

    def getDirByPath(self, path: str):
        path = os.path.normpath(path.lstrip("/")).split(os.path.sep)
        current = self
        for part in path:
            current = current[part]
        return current














# Example usage
# root = Directory()
# root['item1']['item2']['item3'] = Directory()
# root['item1']['item2']['item3'].size = 1024
# root['item1'].created_at = '2023-01-01'
# print(root['item1']['item2']['item3'].size)  # Outputs: 1024
# print(root['item1'].created_at)  # Outputs: 2023-01-01
# print(root) # Outputs: metadata and children

# returns tuple of (stdout, stderr) from mega-pwd
def mega_pwd():
    p = Popen("mega-pwd.bat", cwd=MEGAPATH)
    stdout, stderr = p.communicate()
    return (stdout, stderr)


# runs mega-login, and returns you the exit code
def mega_login(username, password):
    return os.system(f"mega-login {username} {password}")


# runs mega-logout, and returns you the exit code
def mega_logout():
    return os.system(r"mega-logout")


# runs mega-cd on the path given, and returns you the exit code
def mega_cd(path):
    return os.system(f"mega-cd {path}")


# takes 0 or more arguments to be passed (raw) as cmdline arg's to mega-ls
# returns (stdout, stderr) from mega-ls
def mega_ls(*args):
    cmd = ["mega-ls.bat"]
    cmd.extend([arg for arg in args])
    p = Popen(cmd, cwd=MEGAPATH, stdout=PIPE, stderr=PIPE, text=True)
    stdout, stderr = p.communicate()
    return (stdout, stderr)


# takes a string of ls output, and returns a dict of the different fields
def parseLsOutput(line):
    fields = {}
    fields["flags"] = line[0:4].lstrip()
    fields["version"] = line[4:9].lstrip()
    fields["size"] = int(line[9:20].lstrip()) if fields["flags"][0] == '-' else line[9:20].lstrip()
    fields["type"] = "file" if fields["flags"][0] == '-' else "folder" # come back later and turn these into enums
    fields["date"] = line[21:39].lstrip()
    fields["name"] = line[40:].lstrip()
    return fields


# returns a Directory() reflecting the file structure in mega
def buildTreeFromMega(path="/"):
    # preprocessing to make sure the path ends with a slash
    lastChar = path[-1:] if path else '' # grab last char, or empty string, if input is empty string
    path = path + '/' if lastChar != '/' else path

    root = Directory(path="/", numDescendants=0, size=0, name="", type="folder")
    initialPathItems = path.rstrip("/").split("/")
    rootSetup = root
    for item in initialPathItems:  # iterate through each item of the parameter path, and build out root accordingly
        rootSetup[item] = Directory(name=item)
    #root.name = "" if path == '/' else os.path.basename(path.rstrip('/'))

    queue = [root]  # used for BFS traversal through mega to build up Directory tree
    stack = [root]  # used for reverse (bottom-up) traversal through Directory tree to count descendants
    calls = 0

    # iterating through mega file system to build out Directory tree in memory
    while len(queue) > 0:
        folder = queue.pop(0)
        print(f"now scanning contents of: {folder.path}")
        # run mega-ls to get contents of folder
        lsOutput, stderr = mega_ls(folder.path, "-l", "--time-format=ISO6081_WITH_TIME")
        calls += 1


        # parse mega-ls output into a Directory() object
        # skip first 2 lines of mega-ls output, as those are the current folder title, and column headers
        for line in lsOutput.splitlines()[2:]:
            # get dict of child fields
            itemFields = parseLsOutput(line)
            itemFields["path"] = folder.path + itemFields["name"] + "/"
            itemFields["numDescendants"] = 0

            # create Directory() object for child, and apply fields to object
            child = Directory(**itemFields)
            print(child.name)

            # building out the directory tree by appending the child folder
            folder[child.name] = child
            #child[".."] = folder
            #print("".join(f"{field}:{item[field]} (type:{type(item[field])})\n"
            #              for field in item if field == "type" or field == "name"))

            #print(f'Now child is: {child}')
            if child.type == "folder":
                queue.append(child)

            nextFolder = queue[0].path if len(queue) > 0 else "empty queue"
            # input(f'next folder: {nextFolder}\nPress any button to continue...\n')

            stack.append(child)



    '''print("!!!now counting descendants by going bottom-up!!!")
    time.sleep(2)

    # iterating through Directory tree in reverse order (bottom up) to count descendants of each node
    line2 = getCursorPosition().Y
    while len(stack) > 0:
        setCursorPosition(0, line2)
        clearLine()
        current = stack.pop()
        print(f"current item: {current.path}", end='', flush=True)
        parent = current[".."]
        if current.type == "file":
            current.numDescendants = 0
            parent.numDescendants += 1
        elif current.type == "folder":
            parent.numDescendants += current.numDescendants
        setCursorPosition(0, line2)
        clearLine()
    print()
    '''




    print(f"mega calls: {calls}")

    return root



stdout = -11
HANDLE = wintypes.HANDLE
COORD = wintypes._COORD
DWORD = wintypes.DWORD
CHAR = ctypes.c_char
WORD = wintypes.WORD

kernel32 = ctypes.windll.kernel32
GetConsoleScreenBufferInfo = kernel32.GetConsoleScreenBufferInfo
SetConsoleCursorPosition = kernel32.SetConsoleCursorPosition
FillConsoleOutputCharacter = kernel32.FillConsoleOutputCharacterA
FillConsoleOutputAttribute = kernel32.FillConsoleOutputAttribute
GetConsoleCursorInfo = kernel32.GetConsoleCursorInfo
SetConsoleCursorInfo = kernel32.SetConsoleCursorInfo
stdoutHandle = kernel32.GetStdHandle(stdout)



class CONSOLE_SCREEN_BUFFER_INFO(ctypes.Structure):
    _fields_ = [
        ("dwSize", COORD),
        ("dwCursorPosition", COORD),
        ("wAttributes", WORD),
        ("srWindow", wintypes.SMALL_RECT),
        ("dwMaximumWindowSize", COORD)
    ]

class CONSOLE_CURSOR_INFO(ctypes.Structure):
    _fields_ = [
        ("dwSize", wintypes.DWORD),
        ("bVisible", wintypes.BOOL)
    ]



def moveCursorUp(n=1):
    currentPos = getCursorPosition()
    newY = max(0, currentPos.Y - n)
    setCursorPosition(currentPos.X, newY)



def moveCursorDown(n=1):
    currentPos = getCursorPosition()
    consoleInfo = CONSOLE_SCREEN_BUFFER_INFO()
    GetConsoleScreenBufferInfo(stdoutHandle, ctypes.byref(consoleInfo))
    maxY = consoleInfo.dwSize.Y - 1
    newY = min(maxY, currentPos.Y + n)
    setCursorPosition(currentPos.X, newY)


def getConsoleWidth():
    consoleInfo = CONSOLE_SCREEN_BUFFER_INFO()
    GetConsoleScreenBufferInfo(stdoutHandle, ctypes.byref(consoleInfo))
    return consoleInfo.dwSize.X

def setCursorPosition(x, y):
    position = COORD(x, y)
    SetConsoleCursorPosition(stdoutHandle, position)

def getCursorPosition():
    consoleInfo = CONSOLE_SCREEN_BUFFER_INFO()
    GetConsoleScreenBufferInfo(stdoutHandle, ctypes.byref(consoleInfo))
    return consoleInfo.dwCursorPosition

def setCursorX(n):
    currentPos = getCursorInfo()
    width = getConsoleWidth()
    try:
        assert isinstance(n, int), "Input must be integer"
    except AssertionError as e:
        return e
    newX = 0
    if n < 0:
        newX = 0
    elif currentPos.X > width:
        newX = width
    else:
        newX = n
    setCursorPosition(newX, currentPos.Y)

def rightShiftCursor(n):
    currentPos = getCursorInfo()
    width = getConsoleWidth()
    newX = 0
    try:
        assert isinstance(n, int), "Input must be integer"
    except AssertionError as e:
        return e
    if int(n) + currentPos.X < 0:
        newX = 0
    elif n + currentPos.X > width:
        newX = width
    else:
        newX = n + width
    setCursorPosition(newX, currentPos.Y)


def getCursorInfo():
    cursor = CONSOLE_CURSOR_INFO()
    GetConsoleCursorInfo(stdoutHandle, ctypes.byref(cursor))
    return cursor

def showCursor():
    cursor = getCursorInfo()
    newCursor = CONSOLE_CURSOR_INFO(dwSize=cursor.dwSize, bVisible=True)
    kernel32.SetConsoleCursorInfo(stdoutHandle, ctypes.byref(newCursor))

def hideCursor():
    cursor = getCursorInfo()
    newCursor = CONSOLE_CURSOR_INFO(dwSize=cursor.dwSize, bVisible=False)
    kernel32.SetConsoleCursorInfo(stdoutHandle, ctypes.byref(newCursor))


def clearLine():
    pos = getCursorPosition()
    setCursorPosition(0, pos.Y)
    consoleInfo = CONSOLE_SCREEN_BUFFER_INFO()
    GetConsoleScreenBufferInfo(stdoutHandle, ctypes.byref(consoleInfo))
    width = consoleInfo.dwSize.X
    numWritten = DWORD()
    FillConsoleOutputCharacter(stdoutHandle, CHAR(b' '), DWORD(width), COORD(0, pos.Y), ctypes.byref(numWritten))

def clearLineFromPosition(x, y=None):
    width = getConsoleWidth()
    currentPos = getCursorPosition()
    targetY = y if y is not None else currentPos.Y
    setCursorPosition(x, targetY)
    numCharactersModified = DWORD()
    FillConsoleOutputCharacter(stdoutHandle, CHAR(b' '), DWORD(width - x), COORD(x, targetY), ctypes.byref(numCharactersModified))
    setCursorPosition(currentPos.X, currentPos.Y)



def getDuplicateFiles(dirObj, osPath):
    print(f"OS Search Path: {osPath}")
    #osSearchTimeHeader = "OS search time: "
    #print(osSearchTimeHeader)
    duplicates = []  # list of 3-tuples of (fileName, megaFsPath, osPath)
    line0 = getCursorPosition().Y  # progress bar
    line1 = line0 + 1
    line2 = line1 + 1
    line3 = line1 + 2
    scannedMegaCount = 0
    totalMegaDirectories = 5167
    line0Header = "Progress: "
    setCursorPosition(0, line0)
    print(line0Header, end='', flush=True)
    line1Header = "mega file: "
    setCursorPosition(0, line1)
    print(line1Header, end='', flush=True)  # 1. we print line1Header
    line2Header = "OS file: "
    setCursorPosition(0, line2)  # 2. we move cursor down to (0, line2)
    print(line2Header, end='', flush=True)  # 3. we print line2Header
    osStart = 0
    osEnd = 0


    queue = [dirObj]
    while len(queue) > 0:
        current = queue.pop(0)
        children = current.keys()  # for root, this will be all of the immediate descendants
        for child in children:
            setCursorPosition(len(line0Header), line0)
            clearLineFromPosition(len(line0Header))
            print(f"{scannedMegaCount}/{totalMegaDirectories} directories ({(scannedMegaCount / totalMegaDirectories) * 100:.2f}%)")  #TODO: make this based on files, not total directories
            scannedMegaCount += 1
            setCursorPosition(len(line1Header), line1)  # 4. we move cursor back up to (len(line1Header), line1)
            clearLineFromPosition(len(line1Header))  # 5. (I think) we [successfully] clear the line from that position
            print(current[child].path, end='', flush=True)  # 6. we successfully print mega file being examined
            if current[child].type == "folder":  # case child is folder, just add to BFS queue
                queue.append(current[child])
            else:  # case where child is file, look through OS to see if it is duplicated
                # searching OS--not making separate function because it's relatively simple, albeit very nested
                # this could be converted into a separate function to allow searching of any winshell namespace location
                # (including iPhone plugged in. It could provide a universal interface)
                #osStart = time.time()
                for currentOsPath, folders, files in os.walk(osPath):
                    for file in files:
                        osFilePath = os.path.join(currentOsPath, file)
                        setCursorPosition(len(line2Header), line2)
                        clearLineFromPosition(getCursorPosition().X)  # 7. we clear line2 and set cursor where it needs to be
                        print(osFilePath, end='', flush=True)  # 9. we successfully print the osFile being examined
                        if file == child:
                            setCursorPosition(0, line3)  # move down to 3rd line
                            #print(f"Duplicate found! \"{file}\"", end = '', flush=True)  # writing to 3rd line, briefly
                            #time.sleep(.5)
                            duplicates.append((child, current[child].path, currentOsPath))
                            #clearLine()  # clear 3rd line
                            setCursorPosition(len(line2Header), line2)  # move back up to 2nd line
                            break  # found duplicate, so no need to look through rest of this folder
                #osEnd = time.time()
                #setCursorPosition(len(osSearchTimeHeader), line0 - 1)
                #print(osEnd - osStart, end='', flush=True)
                #clearLineFromPosition(len(osSearchTimeHeader), line0 - 1)
                clearLineFromPosition(len(line2Header), line2)
    # tearing down headers
    setCursorPosition(0, line2) # set cursor to second line
    clearLine()  # clear second line
    setCursorPosition(0, line1)  # move back up to 1st line
    clearLine()
    print(f"Duplicate search complete. Found {len(duplicates)} duplicate(s)")
    return duplicates







# writes dict to json file on dict.
def cacheDirectoryInPickle(dataObject):
    fileName = "megaDirectoryCache.pkl"

    if os.path.exists(fileName):
        userInput = input(f"{fileName} already exists. Do you want to overwrite it? (yes/no): ")
        if userInput.lower() != 'yes':
            print("Operation cancelled.")
            return

    with open(fileName, 'wb') as pickleFile:
        pickle.dump(dataObject, pickleFile)
        print(f"Data successfully written to {fileName}")




def loadFromCache():
    fileName = "megaDirectoryCache.pkl"

    if not os.path.exists(fileName):
        print(f"{fileName} does not exist.")
        return None

    with open(fileName, 'rb') as pickleFile:
        dataObject = pickle.load(pickleFile)
        print(f"Data successfully loaded from {fileName}")
        return dataObject


def itemAndDirectoryTests():
    print("====trying Item object====")
    itemPass = 0
    itemFail = 0
    itemTestCount = 0
    myItem = Item(size=5)
    myItem.name = "item!!!!!!name"
    print(f"myItem.size:{myItem.size}")
    print(f"myItem.name:{myItem.name}")
    try:
        itemTestCount += 1
        print(f'attempting to print myItem directly')
        print(f'myItem: {myItem}')
        print(f"success!")
        itemPass += 1
    except Exception as e:
        print(f"failed with error:{e}")
        itemFail += 1
    try:
        itemTestCount += 1
        print(f"attempting to access undeclared item attribute")
        print(f"myItem.field:{myItem.field}")
        print(f"failed to deny access to field")
        itemFail += 1
    except Exception as e:
        print(f"success! denied access to field with error:{e}")
        itemPass += 1
    print(f"passed {itemPass}/{itemTestCount} Item tests")
    print("====trying Directory object====")
    pass_ = 0
    fail = 0
    testCount = 0
    root = Directory(name="root")
    try:
        testCount += 1
        print(f"attempting to print root")
        print(f"root: {root}")
        print(f"success!")
        pass_ += 1
    except Exception as e:
        print(f"failed to print root after creation with error:{e}")
        fail += 1
    try:
        testCount += 1
        print(f"attempting to add path attribute to root")
        root.path = "/"
        print(f"root with path added: {root}")
        print(f"success!")
        pass_ += 1
    except Exception as e:
        print(f"failed to add path attribute with error:{e}")
        fail += 1
    try:
        testCount += 1
        print(f'attempting to access path attribute of root')
        print(f'root.path: {root.path}, root.name: {root.name}')
        print(f"success!")
        pass_ += 1
    except Exception as e:
        print(f"failed to access path attribute with error:{e}")
        fail += 1
    try:
        testCount += 1
        print(f"attempting to add child")
        root["subfolder1"] = Directory()
        print(f"root after adding child: {root}")
        print(f"success!")
        pass_ += 1
    except Exception as e:
        print(f"failed to add child with error:{e}")
        fail += 1
    try:
        testCount += 1
        print(f"attempting to add attributes to child")
        root["subfolder1"].name = "subfolder1"
        root["subfolder1"].path = "/subfolder1"
        print(f'root["subfolder1"]:{root["subfolder1"]}')
        print(f"success!")
        pass_ += 1
    except Exception as e:
        print(f"failed to add attributes to child with error:{e}")
        fail += 1
    try:
        testCount += 1
        print(f"attempting to access child")
        subfolder1 = root["subfolder1"]
        print(f'subfolder1: {subfolder1}')
        print("success!")
        pass_ += 1
    except Exception as e:
        print(f"failed to access child with error:{e}")
        fail += 1
    try:
        testCount += 1
        print(f"attempting to add grandchild")
        root["subfolder1"]["subsubfolder1"] = Directory(name="subsubfolder1", path="/subfolder1/subsubfolder1")
        print(f'root["subfolder1"]:{root["subfolder1"]}')
        print(f"success!")
        pass_ += 1
    except Exception as e:
        print(f"failed to add grandchild with error:{e}")
        fail += 1
    try:
        testCount += 1
        print(f"now attempting to add sibling to subfolder1")
        root["subfolder2"] = Directory(name="subfolder2", path="/subfolder2")
        print(f"root:{root}")
        print("success!")
        pass_ += 1
    except Exception as e:
        print(f"failed to add sibling to root with error:{e}")
        fail += 1
    try:
        testCount += 1
        print(f'now attempting to access attribute of subfolder1 that doesn\'t exist')
        print(f'subfolder1.size: {root["subfolder1"].size}')
        print(f'failed to deny access to undeclared attribute.')
        fail += 1
    except AttributeError as e:
        print(f'success! attempt to access subfolder1.size denied as:{e}')
        pass_ += 1
    try:
        testCount += 1
        print(f'now attempting to access child that doesn\'t exist')
        print(f'root["nonexist"]:{root["nonexist"]}')
        print(f'failed to deny access to undeclared child')
        fail += 1
    except KeyError as e:
        print(f'success! attempt to access root["nonexist"] denied as:{e}')
        pass_ += 1
    print(f"passed {pass_}/{testCount} Directory tests")